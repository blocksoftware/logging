module.exports = function(app){

    var express = require("express")
    var serverConfig = require('./config/default/defaultConfig');
    var util = require('util');
    var fs = require('fs');
    var os=require('os');
    var rp = require('request-promise');

    //Cache for local storage if needed in the future
    const NodeCache = require( "node-cache" );
    const notifySourceCache = new NodeCache( { stdTTL: serverConfig.options.notifyTtl, checkperiod: serverConfig.options.notifyCheckPeriod } );
    const currentDeviceCache = new NodeCache( { stdTTL: serverConfig.options.currentDeviceTtl, checkperiod: serverConfig.options.currentDeviceCheckPeriod } );
    const uniqueDevicesCache = new NodeCache( { stdTTL: serverConfig.options.uniqueDeviceTtl, checkperiod: serverConfig.options.uniqueDeviceCheckPeriod } );

    var pkg;
    var notifyCounter = 0;
    var infoSummaryUpdateDate = new Date();
    var infoDetailUpdateDate = new Date();
    const LOG_SUMMARY_INFO_UPDATE_INTERVAL = serverConfig.options.logSummaryInfoStatsInterval * 1000;
    const LOG_DETAIL_INFO_UPDATE_INTERVAL = serverConfig.options.logDetailInfoStatsInterval * 1000;

    const fwUrl = "http://10.116.27.206:8088/services/collector/event"
    
    pkg = require('./package.json');


    var eventListener = express();
    var https = require('https')
    var http = require('http')

    var sslConfig = require('../ssl-config');
    var options = {
      key: sslConfig.privateKey,
      cert: sslConfig.certificate,
    };

    var bodyParser = require('body-parser')

     //eventListener.use(bodyParser.urlencoded({ extended: true }));
     eventListener.use(bodyParser.json({limit: '4096mb'}));
     eventListener.use(bodyParser.urlencoded({limit:'4096mb', extended: true}))

     console.log("Creating event listener")
    //-----------------------------------------------------------------------
    //Post Listener: DNA Center Barts
    //
    //Description: Listents for events on /networkEvent from DNA center
    //-----------------------------------------------------------------------

    eventListener.post('/networkEvent', function(req, res) {

        console.log("the below Network event was RECIEVED:")
//remove comment to see request body in forever logs
//console.log(req.body)
        
	var bodyData = req.body;
        var object = {
          "sourcetype": "DNA network Event",
            "event": bodyData
        }

         const postToSplunk = {
            method: "POST",
            uri: fwUrl,
            body: object,
            json: true,
            headers: {
                'Authorization': 'Splunk ba081708-538a-46ff-9dc1-859bf6a645bc'
            },
        }

        rp(postToSplunk).then(function(response){
            console.log(response)

        }).catch(function(error){
            console.log("ERROR RECIEVED FROM POSTING TO SPLUNK!")
            console.log(error.error)
        })


        return res.sendStatus(200);
    });



   //-----------------------------------------------------------------------
   //Post Listener: DNA Center
   //
   //Description: Listents for events on /swimEvent from DNA center
   //-----------------------------------------------------------------------

   eventListener.post('/swimEvent', function(req, res) {
       
	console.log("the below SWIM event was RECIEVED:")
//remove comment below to see request body in forever logs
//	console.log(req.body)

	var bodyData = req.body;
	var object = {
           "sourcetype": "DNA Swim Event",
           "event": bodyData
       }

        const postToSplunk = {
           method: "POST",
           uri: fwUrl,
           body: object,
           json: true,
           headers: {
               'Authorization': 'Splunk 7dffc3a2-e022-4dfb-b39f-09bea36ebcfc'
           },
       }

       rp(postToSplunk).then(function(response){
           console.log(response)

       }).catch(function(error){
           console.log("ERROR RECIEVED FROM POSTING TO SPLUNK!")
           console.log(error.error)
       })


       return res.sendStatus(200);
   });

   
    //-----------------------------------------------------------------------
    //Handler: SIGINT
    //
    //Description: Interrupt handler for Ctrl+C. This will start the delete of the
    //             existing notification subscritpion
    //-----------------------------------------------------------------------
    process.on('SIGINT', function() {
        logger.info("\nGracefully shutting down from SIGINT (Ctrl+C)");
    	process.exit(0);
    });

    //-----------------------------------------------------------------------
    //Function: runMain
    //
    //Description: Main function to start script
    //
    //Parameters: None
    //
    //Returns: None
    //-----------------------------------------------------------------------
    function runMain() {
        //eventListener.listen(serverConfig.options.eventListenerPort);
        //console.log(" listening on HTTP port " + serverConfig.options.eventListenerPort);

       var eventServer = https.createServer (options, eventListener);
      // var eventServer = http.createServer (eventListener);
       console.log("Event Server Created")
       eventServer.listen(serverConfig.options.eventListenerPort);
       console.log(" listening on HTTPS port " + serverConfig.options.eventListenerPort);


    }
    runMain();
};
